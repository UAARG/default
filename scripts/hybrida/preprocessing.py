from hybrida.core import *
from copy import deepcopy
from typing import Type
import numpy as np

# TODO: Add a function to scale the map


def discretize(map_input: Map, node: Type[Node], theta_resolution: int = 24) -> Map:
    """
    Discretize a map with corresponding node type. Hasn't been put to use yet.
    """
    map_clone = deepcopy(map_input)
    x_size = map_clone.x_size
    y_size = map_clone.y_size

    if node is Node:
        map_clone.nodes = np.zeros((x_size, y_size))
        for x in range(x_size):
            for y in range(y_size):
                map_clone.nodes[x, y] = Node(x, y)
    elif node is ThetaNode:
        map_clone.theta_resolution = theta_resolution
        map_clone.nodes = np.zeros((x_size, y_size, theta_resolution))
        for x in range(x_size):
            for y in range(y_size):
                for i in range(theta_resolution):
                    map_clone.nodes[x, y, i] = ThetaNode(x, y, i, theta_resolution)

    return map_clone


def resize(_map: Map, scale: Tuple[float, float], plane: Plane = None, waypoints: List[Tuple[float, float]] = None):
    x_size = np.ceil(_map.x_size * scale[0])
    y_size = np.ceil(_map.y_size * scale[1])
    scale = np.array(scale)
    obstacles = []
    for obstacle in _map.obstacles:
        temp = np.array(obstacle)
        temp = scale * temp
        obstacles.append(temp.tolist())
    package = [Map(x_size, y_size, obstacles)]

    if plane:
        width = plane.width * scale[0]
        radius = plane.turn_radius * scale[0]
        package.append(Plane(width, radius))

    if waypoints:
        waypoints = (scale*np.array(waypoints)).tolist()
        package.append(waypoints)
    return package
