# TODO: [IMPORTANT] Normalize all cost function's output, or add a normalization function that takes weights as an input


from hybrida.core import *
from typing import Tuple
from shapely.geometry import LineString
import numpy as np


def euclidean(start: Node, goal: Node) -> float:
    """
    Calculate the euclidean distance between two points

    Parameter
    ---------
    start : Node
        the first point's coordinate

    goal : Node
        the second point's coordinate

    Return
    ------
    distance : float
        euclidean distance between start and goal
    """

    (x0, y0) = start.continuous_state[:-1] if hasattr(start, 'continuous_state') else start.xy
    (x1, y1) = goal.continuous_state[:-1] if hasattr(goal, 'continuous_state') else goal.xy

    return math.sqrt(math.pow(x0 - x1, 2) + math.pow(y0 - y1, 2))


def chebyshev(start: Tuple[int, int], goal: Tuple[int, int], D: float = 1.0, D2: float = 1.0) -> float:
    """
    Chebyshev distance heuristic between two nodes with discretized coordinate.
    Distance is calculated as if one can move one square either adjacent or diagonal.

    Parameter
    ---------
    start : Tuple[int, int]
        the first node's discretized coordinate

    goal : Tuple[int, int]
        the second node's discretized coordinate

    D : float
        move cost to adjacent node

    D2 : float
        move cost to diagonal node

    Return
    ------
    distance : float
        chebyshev distance between start and goal
    """

    dx = abs(start[0] - goal[0])
    dy = abs(start[1] - goal[1])
    return D * (dx + dy) + (D2 - 2 * D) * min(dx, dy)


def check_collision(path, obstacles, width):
    """Check whether the path collides with the obstacles"""
    if obstacles:
        line = LineString(path)
        for obstacle in obstacles:
            obstacle_line = LineString(obstacle).buffer(width)
            if line.intersection(obstacle_line):
                return True
    return False


def move_cost(path, obstacles, width):
    """
    A move cost function that returns the path length as cost, collision with an obstacle spikes the cost
    """
    path = np.array(path)
    offset_path = np.roll(path, 1, axis=0)
    distances = path[1:] - offset_path[1:]
    cost = 0
    for i in distances:
        cost += np.linalg.norm(i)

    # cost *= 1.01 if len(path) > 2 else 1

    if check_collision(path, obstacles, width) is True:
        return cost * 100

    return cost


def momentum_move_cost(path, radius, obstacles, width, previous_momentum, previous_radius):
    """
    A move cost function that return higher cost if current turning radius is different from the previous one.
    "Momentum" is gradually built up when the plane moves down a path with the same turning radius, higher momentum
    results in a lower cost when the path length is the same
    """
    path = np.array(path)
    offset_path = np.roll(path, 1, axis=0)
    distances = path[1:] - offset_path[1:]
    cost = 0
    for i in distances:
        cost += np.linalg.norm(i)

    if round(radius, 3) == round(previous_radius, 3):
        momentum = previous_momentum + 1
    else:
        momentum = 0

    cost *= (momentum + 2) / (momentum + 1)

    if check_collision(path, obstacles, width) is True:
        return cost * 100

    return cost, momentum
