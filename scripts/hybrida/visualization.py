from typing import Tuple, List, Set, Optional
import numpy as np
from hybrida.core import *
import matplotlib.pyplot as plt


# def arc(start: Tuple[float, float, float], goal: Tuple[float, float, Optional[float]], radius: Optional[float] = None,
#         resolution: float = np.pi/180) -> List[Tuple[float, float]]:
#     """
#     Draw an arc between two points given their positions, initial heading, and turning radius. Depleted.
#     """
#     raise Warning("This function is depleted")
#     # Determine turning direction
#     theta1 = start[2]
#     heading_vector = np.array([np.cos(theta1), np.sin(theta1)])
#     relative_position_vector = np.array([goal[0] - start[0], goal[1] - start[1]])
#     turning_direction = np.cross(heading_vector, relative_position_vector)
#
#     if turning_direction == 0:  # Going straight
#         return [start[:2], goal[:2]]
#
#     # Generate radius if not entered
#     if radius is None:
#         d = np.linalg.norm(relative_position_vector)
#         dot_product = np.dot(heading_vector, relative_position_vector)
#         norm_product = np.linalg.norm(heading_vector) * d
#         gamma = np.arccos(dot_product / norm_product)  # Angle between original heading and vector of relative position
#         # Epsilon: angle between original heading's normal vector and the relative position vector
#         epsilon = np.pi / 2 - gamma
#         radius = d / (2 * np.cos(epsilon))
#     else:
#         epsilon = np.arccos(np.linalg.norm(np.array(start[:2]) - np.array(goal[:2])) / (2 * radius))
#
#     if turning_direction > 0:  # Turning left
#         sigma = theta1 + np.pi / 2
#         theta2 = theta1 + 2 * epsilon
#     elif turning_direction < 0:  # Turning right
#         sigma = theta1 - np.pi / 2
#         theta2 = theta1 + 2 * epsilon
#
#     # Check if goal's heading is correct if entered
#     if len(goal) == 3 and theta2 != goal[2]:
#         raise RuntimeError("heading ")
#
#     # Generate arc
#     x0 = start[0] + radius * np.cos(sigma)
#     y0 = start[1] + radius * np.sin(sigma)
#     alpha = sigma + np.pi
#     beta = goal[2] + np.pi / 2
#     path = [start[:2]]
#     for angle in np.arange(alpha, beta, resolution):
#         path.append((x0 + np.cos(angle), y0 + np.sin(angle)))
#     path.append(goal[:2])
#     return path


# def arc(start: Tuple[float, float], goal: Tuple[float, float], radius: float, direction: float,
#         reverse: bool = False, resolution: float = np.pi/180) -> List[Tuple[float, float]]:
#     """
#     TODO: Fix the algorithm that generate the arc so that it works properly
#     NOTE: depleted
#
#     Draw an arc between two points given their positions, turning radius and turning direction
#
#     Params
#     ------
#     start : Tuple[int, int]
#         starting coordinate of the arc
#
#     goal : Tuple[int, int]
#         ending coordinate of the arc
#
#     radius: float
#         radius of the arc
#
#     direction : float
#         turing direction from the starting point, positive is turing counterclockwise, negative is turing clockwise
#
#     resolution : float
#         step size of angle change when calculating the arc, e.g., resolution = pi/180 results in 360 segments of lines
#         for a 360 degree turn
#
#     Return
#     ------
#     path : List[Tuple[float, float]]
#         a list of coordinates describing the arc, including the starting and ending coordinates
#     """
#
#     # Calculate centre position
#     raise Warning("This function is depleted")
#     p1 = np.array(start)
#     p2 = np.array(goal)
#     vd = (p2 - p1).reshape(1, -1).T  # Vector pointing from starting point to ending point
#     d = np.linalg.norm(vd)
#     theta = direction / np.absolute(direction) * np.arccos(d / (2 * radius))
#     if reverse is True:
#         theta = -theta
#     transform_matrix = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
#     vr = np.matmul(transform_matrix, vd / d) * radius
#     centre = (p1 + vr.T).reshape(-1)
#
#     # Generate arc
#     vp1 = p1-centre
#     vp2 = p2-centre
#     alpha = np.angle([complex(*vp1)])
#     beta = np.angle([complex(*vp2)])
#
#     path = list()
#     # print(beta < alpha)
#     print(alpha)
#     print(beta)
#     if beta < alpha:
#         alpha -= 2 * np.pi
#     print(alpha)
#     print(beta)
#
#     if reverse is True:
#         (alpha, beta) = (beta, alpha + 2 * np.pi)
#     print(alpha)
#     print(beta)
#
#     for angle in np.arange(alpha, beta+resolution, resolution):
#         path.append((centre[0] + np.cos(angle), centre[1] + np.sin(angle)))
#     return path


def visualize(_map: Map, paths: List[List[Tuple[float, float]]], start: Optional[Node] = None,
              goal: Optional[Node] = None, open_vertices: Optional[Set[Node]] = None,
              closed_vertices: Optional[Set[Node]] = None):
    """
    TODO: 1. add visualization for current path and node, decrease alpha for other paths, it gets real messy after some
        iterations
        2. Figure out a way that visualize the entire search process from start to goal in the same figure.
    Visualize the playground

    Params
    ------
    _map : Map
        A Map object that contains information about obstacles and the size of the playground,
        default style for obstacles: black 'k-'

    paths : List[List[Tuple[float, float]]]
        A list of paths that will be plotted onto the graph, default style: blue '-'

    start : Node
        starting point of the path, default color: green

    goal : Node
        goal of the search, default color: red

    open_vertices : Set[Node]
        a set of nodes that have been explored, default color: yellow

    closed_vertices : Set[Node]
        a set of nodes that have been explored exhaustively, default color: black
    """

    # Initialize axis
    x_range = _map.x_size
    y_range = _map.y_size
    ax = plt.subplot()
    ax.set_xlim(-1, x_range + 1)
    ax.set_xticks(np.arange(0, x_range + 2, 1))
    ax.set_ylim(-1, y_range + 1)
    ax.set_yticks(np.arange(0, y_range + 2, 1))
    ax.grid('on')
    ax.set_aspect('equal', adjustable='box')

    # Plot obstacles
    for obstacle in _map.obstacles:
        ax.plot(*list(zip(*obstacle)), 'k-')

    # Plot paths
        for path in paths:
            ax.plot(*list(zip(*path)), 'b-', alpha=0.5)

    # Plot start and goal
    if start is not None:
        start_coord = start.continuous_state[:-1] if hasattr(start, 'continuous_state') else start.xy
        plt.scatter(*start_coord, c='green')

    if goal is not None:
        goal_coord = goal.continuous_state[:-1] if hasattr(goal, 'continuous_state') else goal.xy
        plt.scatter(*goal_coord, c='red')

    # Plot open and closed vertices
    if open_vertices is not None:
        for node in open_vertices:
            coord = node.continuous_state[:-1] if hasattr(node, 'continuous_state') else node.xy
            plt.scatter(*coord, c='yellow')

    if closed_vertices is not None:
        for node in closed_vertices:
            coord = node.continuous_state[:-1] if hasattr(node, 'continuous_state') else node.xy
            plt.scatter(*coord, c='black')
    plt.show()


if __name__ == "__main__":
    test_start = (1, 1)
    test_goal = (0, 1)
    test_path = np.array(arc(test_start, test_goal, 1, 1, True))
    # print(test_path)
    ax = plt.subplot()
    ax.scatter(*test_start, c='green')
    ax.scatter(*test_goal, c='red')
    ax.plot(test_path[:, 0], test_path[:, 1])
    ax.set_aspect('equal')
    plt.show()
