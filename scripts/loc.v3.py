from math import sin
from math import cos

xOrigin, yOrigin = map(int, input("Location (x y): ").split()) #starting point
maxRadius = int(input("Radius: "))
#set distance between each ring of the spiral
#adjust so that there are fewer points on the inner
#spirals and more points on the outser (angle changes less as 
# radius goes up)

#These get incremented to generate the spiral
currentRadius = 0
theta = 0

numPoints = 400 #Number of points in the spiral

#This list will get populated with points
listt = []

radiusIncrement = maxRadius / numPoints

while currentRadius < maxRadius:
    
    x = xOrigin + (currentRadius * cos(theta))
    y = yOrigin + (currentRadius * sin(theta))
    
    point = (x,y)
    listt.append(point)
    
    currentRadius = currentRadius + radiusIncrement
    theta = theta + 0.1
    
    
f = open('points.txt','w')
for item in listt:
    f.write("%s\n" % str(item))

f.close()
