# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 18:38:26 2020

@author: Alfred
"""

import numpy as np
import matplotlib.pyplot as plt

class LocationSearch():
    def __init__(self):
        pass

    def search_for_person(self, loc,radius,size):
        radius0 = size/100
        radiuslist= [radius0]   
        currentradius = radius0
        currentangle = 0
        angles = [0]
        while currentradius < radius:
            currentangle += size/(2*np.sqrt(currentradius))
            angles.append(currentangle)
            currentradius = size/(2*np.pi)*currentangle
            radiuslist.append(currentradius)
        
        x = []
        y = []
        for n, i in enumerate(radiuslist):
            x.append(i*np.cos(angles[n]))
            y.append(i*np.sin(angles[n]))
        plt.plot(x, y, 'k-')
        plt.show()

    
