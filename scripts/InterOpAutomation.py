# -*- coding: utf-8 -*-
"""
This is a script written by the UAARG Autopilot team in order to take the JSON file
of the InterOp data, and form a mission out of the information provided.


Details on connections available at:
    https://github.com/auvsi-suas/interop#interop-integration


Written October 10, 2019
"""

import json, csv, requests
from sklearn.neighbors import NearestNeighbors as nnbors
import numpy as np
from . import ImageGrid
#from auvsi_suas.client import client
#from auvsi_suas.proto import interop_api_pb2

class GetFile:
    """
    Retrieving the Interop data from the server, and parsing it into a usable
    form for further use and modification
    
    
    Variables:
        urlname: (Temp) the name of either the file for the JSON or the server
        to retrieve the  interop data from
        
        
        
        
        
    Normal sample request (Non python):
        GET /api/missions/1 HTTP/1.1
        Host: 192.168.1.2:8000
        Cookie: sessionid=9vepda5aorfdilwhox56zhwp8aodkxwi
    """
    def __init__(self, urlname, parameters):
        #with open(filename, 'r') as file:
        #    pass
        
        #(Not in Use) Getting JSON file for mission parameters from interop server
        r = requests.get(url=urlname, params=parameters)
        self.data = r.json()
        pass

class WriteFile:
    """
    Writing the mission waypoint file that will be used in the  execution of 
    the actual mission. The  final step in setting up the  mission. 
    
    
    Variables:
        missionname: the name of the mission waypoint file that will be created
        filename: the name of the JSON File that will be used for the mission details
        
    Class Variables:
        self.__missionfile: The  actual Mission file to be sent to MissionPlanner in list form
        self.__ind: index of the row being written
        data = loaded mission from JSON file
        
    """
    
    def __init__(self, missionname, filename):
        #Index for row/waypoint number
        self.__ind = 0
        
        #Title, home-coordinate
        self.__missionfile = [['QGC WPL 110'], [self.__ind, 1, 0, 0, 0, 0, 0, 0, 0, 0, float(0), 1]]
        self.__ind += 1
        with open(filename, 'r') as jsonfile:
            data = json.load(jsonfile)
            print(type(data))
        alt1 = data['waypoints'][0]['altitude']
        
        #Flight Bounds:
        flightbounds = data['flyZones'][0]['boundaryPoints']
        boundpts = []
        for i in flightbounds:
            boundpts.append([i['latitude'], i['longitude']])
        
        
        #Takeoff waypoint writing to file list
        self.__missionfile.append([self.__ind, 0, 3, 22, float(0), float(0), float(0), float(0), float(0), float(0), float(alt1), 1])
        self.__ind += 1
        
        #Writing Flyarround waypoint locations to file list
        for point in data['waypoints']:
            self.printwaypoint(16, point, float(point['altitude']))
            self.__ind += 1
            
        #Vehicle Dropoff
        
        #Search for Person
        dropoff = data['emergentLastKnownPos']
        self.printwaypoint(21, dropoff, float(300))
        self.__ind += 1
        
        #Off-Axis image location
        offaxpt = [data['offAxisOdlcPos']['latitude'], data['offAxisOdlcPos']['longitude']]
        boundaries = np.array(boundpts)
        offaxis = np.array(offaxpt)
        nbrs = nnbors(n_neighbors=2, algorithm='ball_tree').fit(boundaries)
        distance, index = nbrs.kneighbors(offaxis)
        print(distance, index)
        
        
        
        #Image locations
        imagegrid = []
        for gridpoint in data['searchGridPoints']:
            #print(imagegrid)
            imagegrid.append((gridpoint['latitude'], gridpoint['longitude']))
        print(imagegrid)
        imagelocations = ImageGrid(imagegrid, 0.0004)
        print(imagelocations.locs2)
        raise Exception
            #Alfred is working on I/O for this!
            #self.printwaypoint(16, gridpoint, float(300))
            #self.__ind += 1
            #pass
        
        #Set last waypoint as landing in file list
        self.printwaypoint(21, point, float(0))
        self.__ind += 1
        
        
        #Obstacle Avoidance & Check for Out of bounds Flight!
        for i in data['stationaryObstacles']:
            self.printwaypoint(21, i, i['height'])
            self.__ind += 1
            #print(i)

        #Taking the File list and Writing an actual TSV File for use in Mission Planner
        with open(missionname, 'w', newline='') as out_file:
            tsv_writer = csv.writer(out_file, delimiter='\t')
            for line in self.__missionfile:
                tsv_writer.writerow(line)

    #Function to write waypoint of type typedat at location data
    def printwaypoint(self, typedat, point, alt):
        #missionline = []
        self.__missionfile.append([self.__ind, 0, 3, typedat, float(0), float(0), float(0), float(0), float(point['latitude']), float(point['longitude']), alt, 1])

class ObstacleAvoidance:
    """
    A class used to modify the JSON Data in order to form a new path 
    considering the obstacle avoidance.
    
    Temporary in case it is implemented differently
    """
    def __init__(self):
        pass

if __name__ == '__main__':
    
    '''client = client.Client(url='http://127.0.0.1:8000',
                       username='testuser',
                       password='testpass')
    URL = '/api/missions/1 HTTP/1.1'
    PARAMS = {
            'Host': "192.168.1.2:8000", 
            'Cookie': 'sessionid=9vepda5aorfdilwhox56zhwp8aodkxwi'
             }
    '''
    #file = GetFile(URL, PARAMS)
    WriteFile('AutoMissionTest.waypoints', 'dummyjson.JSON')
    #print(file.data)