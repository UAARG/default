# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 17:16:06 2020

@author: vsauve
"""
import numpy as np
import matplotlib.pyplot as plt

class ImageGrid():
    def __init__(self, locs, size):
        self.locs2 = []
        for n,i in enumerate(locs):
            self.locs2.append(i)
            if n < len(locs) - 1:
                line = np.sqrt((locs[n+1][0]-i[0])**2+(locs[n+1][1]-i[1])**2)
                #print(line, size)
                discretization = int(np.floor(line/size))
                for disc in range(discretization - 1):
                    lat = ((locs[n+1][0]-i[0])/discretization)*(disc+1) + i[0]
                    long = ((locs[n+1][1]-i[1])/discretization)*(disc+1) + i[1]
                    self.locs2.append((lat, long))
            else:
            
                line = np.sqrt((locs[0][0]-i[0])**2+(locs[0][1]-i[1])**2)
                #print(line, size)
                discretization = int(np.floor(line/size))
                for disc in range(discretization):
                    lat = ((locs[0][0]-i[0])/discretization)*(disc+1) + i[0]
                    long = ((locs[0][1]-i[1])/discretization)*(disc+1) + i[1]
                    self.locs2.append((lat, long))
            
        #print(self.locs2)
        self.path = []
        if np.mod(len(self.locs2), 2) == 1:
            steps = int(len(self.locs2)/2-0.5)
            sign = 1
            self.path.append(self.locs2[0])
            for i in range(steps):
                #print(i)
                #print(sign)
                #print(self.locs2[sign * (i+1)])
                self.path.append(self.locs2[sign * (i+1)])
                sign = -sign
                #print(sign)
                #print(self.locs2[sign * (i+1)])
                self.path.append(self.locs2[sign * (i+1)])
            
            #print(self.path)
        else:
            steps = int(len(self.locs2)/2)
            sign = 1
            for i in range(steps):
                #print(i)
                #print(sign)
                #print(self.locs2[sign * (i)])
                self.path.append(self.locs2[sign * (i)])
                sign = -sign
                #print(sign)
                #print(self.locs2[sign * (i)])
                self.path.append(self.locs2[sign * (i)])
            pass
        plt.plot([i[0] for i in self.path], [i[1] for i in self.path], 'b-')
        plt.show()
        
            #steps = len(self.locs2)/2
image = ImageGrid([(1,1), (4,1), (4,4), (1,4), (0, 2.5)], 0.5)
#print(image.locs2)



