from hybrida import heuristics
from typing import Tuple, Dict, Optional
import numpy as np
from hybrida.core import *
from hybrida.preprocessing import discretize
import matplotlib.pyplot as plt
from copy import deepcopy
from hybrida.visualization import visualize
from shapely.geometry import LineString
from descartes import PolygonPatch
from hybrida.preprocessing import resize


def get_hybrid_neighbours(current: Tuple[float, float, float], _map: Map, plane: Plane, turning_resolution: int = 1,
                          d: float = None) -> Tuple[List, Dict, Dict]:
    """
    TODO: add solution for when d > turn_radius * 2, edit d documents afterwards
    TODO: calculate theta02 from theta01 based on epsilon instead of theta2, can reduce conditional statement
    TODO: when d < sqrt(2), a step forward in some situation does not generate a neighbour without turning, this causes
        wacky lines as a result
    Move the plane forward in different directions with a certain displacement d at different turning rate, to , explore its
    possible neighbours. Returns a list of the neighbours, a dictionary of the paths leading to each neighbour, and each
    neighbour's corresponding turning radius

    Params
    ------
    current : Tuple[float, float, float]
        current node's coordinate and heading

    _map : Map
        the map that the search is conducted in

    plane : Plane
        the plane used for the search

    turning_resolution : int = 1
        determines the number of different heading direction explored, total number of heading explored is equal to
        turning_resolution * 2 + 1.

    d : float = None
        a fixed displacement that the plane will move, set to equal to turn_radius if none is given, cannot be larger
        than turn_radius * 2

    Returns
    -------
    neighbours : List[Tuple[float, float, float]]
        a list of the explored neighbours

    paths : Dict[Tuple[float, float, float], List[Tuple[float, float]]]
        a dictionary that contains the path that leads to each neighbour from the current point, uses the elements in
        neighbours as its keys

    radii :  Dict[Tuple[float, float, float], float]
        a dictionary that contains the turning radius that leads to each neighbour from the current point, uses the
        elements in neighbours as its keys
    """

    # Calculate and constrain d
    turn_radius = plane.turn_radius
    if not d:
        d = turn_radius
    if d < np.sqrt(2):
        d = np.sqrt(2)
    if d > 2 * turn_radius:
        d = 2 * turn_radius

    # At max turning rate,
    # Epsilon: angle between original heading's normal vector and the relative position vector
    min_epsilon = np.arccos(d / (2 * turn_radius))
    # max_beta = math.pi / 2 - min_epsilon  # Beta: angle between resultant heading and the relative position vector
    max_gamma = np.pi / 2 - min_epsilon  # Gamma: angle between original heading and the relative position vector
    # max_alpha = max_beta + max_gamma  # Alpha: heading change

    # Generate neighbours at different turning rates
    neighbours = list()  # List of neighbours
    radii = dict()  # Each neighbour's turning radius
    paths = dict()  # Each neighbour's path (for visualization purpose)

    (x1, y1, theta1) = current
    if theta1 != 0:
        theta1 = np.abs(theta1) % (2 * np.pi) * np.abs(theta1) / theta1

    num_of_heading = turning_resolution * 2 + 1
    for i in range(num_of_heading):  # For each turning direction
        gamma = -max_gamma + max_gamma / turning_resolution * i
        if gamma != 0:
            epsilon = np.pi / 2 - abs(gamma)
            r = d / 2 / np.cos(epsilon)  # Resulted turning radius
            alpha = 2 * gamma
            sigma = theta1 + gamma
            theta2 = theta1 + alpha  # Resulted neighbour's heading
        else:
            theta2 = sigma = theta1
            r = np.inf
        if theta2 != 0:
            theta2 = np.abs(theta2) % (2 * np.pi) * np.abs(theta2) / theta2

        # Neighbour's coordinate
        x2 = x1 + d * np.cos(sigma)
        y2 = y1 + d * np.sin(sigma)

        neighbour = (x2, y2, theta2)
        print('neighbour:', neighbour)
        if gamma == 0:
            path = [current[:2], neighbour[:2]]
            if heuristics.check_collision(path, _map.obstacles, plane.width) is True:
                continue
            turning_direction = 1
        else:
            # Calculate centre of turning circle
            turning_direction = abs(gamma) / gamma  # positive if turning ccw, negative if cw
            theta10 = theta1 + turning_direction * np.pi / 2
            x0 = x1 + r * np.cos(theta10)
            y0 = y1 + r * np.sin(theta10)

            # Calculate starting and ending angle for the arc
            theta01 = theta1 - turning_direction * np.pi / 2
            theta02 = theta2 - turning_direction * np.pi / 2

            if turning_direction > 0 and theta01 > theta02:
                theta01 -= 2 * np.pi
            elif turning_direction < 0:
                if theta01 < theta02:
                    theta01 += 2 * np.pi
                theta01, theta02 = theta02, theta01

            # Generate turning path
            path = list()
            step_size = np.pi / 180
            # theta02 += step_size if theta02 % step_size else 0
            for angle in np.arange(theta01, theta02, step_size):
                path.append((x0 + r * np.cos(angle), y0 + r * np.sin(angle)))
            if turning_direction < 0:
                path.append(current[:2])
                path.reverse()
            else:
                path.append(neighbour[:2])
            if heuristics.check_collision(path, _map.obstacles, plane.width) is True:
                continue

        print('paths:', paths)
        print('neighbour:', neighbour)
        paths[neighbour] = path
        neighbours.append(neighbour)
        radii[neighbour] = turning_direction * r

    return neighbours, paths, radii


# def transform_to_discrete_theta_node(node: Tuple[float, float, float], theta_resolution: int) -> ThetaNode:
#     """
#     Depleted. Replaced by ThetaNode class method from_coord()
#     """
#     raise Warning('Function depleted, replaced by ThetaNode class method from_coord()')
#     x = int(node[0])
#     y = int(node[1])
#     theta = node[2]
#     theta = theta + 2 * np.pi if theta <= 0 else theta
#     theta_order = theta // (np.pi * 2 / theta_resolution) - 1
#     theta_node = ThetaNode(x, y, theta_order, theta_resolution)
#     theta_node.continuous_state = node
#     return theta_node


def hybrid_search(_map: Map, plane: Plane, start: Tuple[float, float, float], goal: Tuple[float, float, float],
                  heuristic=heuristics.euclidean, search_resolution: int = 6, d: float = None):
    """
    TODO: 1. Currently there exists two systems that tracks f scores, g scores and each node's origin, one exists in the
            function namespace keeping all node's data in a list or dictionary (see f_scores, g_scores. came_from, and
            came_from_path). The other exists in the Node class instance's namespace to track each node's respective
            data (see Node.f_score, .g_score, .came_from, and .path). Try to reduce these systems into one if possible.
            Note that reducing the tracking system in the function namespace might rendering it hard to provide
            information if visualization is required during the search in the future.
        2. Make theta_resolution an argument, or develop the discretize() function in preprocessing module to give the
            Map instance a theta_resolution attribute so that hybrid_search() function can access it.
        3. include the heading of the goal as a metric for cost during the search, so that the search would prioritize
            path that ends in a similar heading as indicated in the input
    Search for path solution using hybrid A* algorithm

    Params
    ------

    _map : Map
        the map that the search is conducted in

    plane : Plane
        the plane used for the search

    start : Tuple[float, float, float]
        location and heading of the starting point

    goal : Tuple[float, float, float]
        location and heading of the goal, note that at this stage the heading of the goal is irrelevant to the search

    heuristic : function
        a heuristic function used to estimate the cost from the current node to the goal

    search_resolution : int
        defines the search density, passed to the turning_resolution argument in get_hybrid_neighbours() function

    d : float
        defines the search displacement in each step, passed to the d argument in get_hybrid_neighbours() function

    Return
    ------

    path : List[Tuple[float, float]]
        a list of coordinates that describe the generated path
    """
    theta_resolution = 36

    # Initialize variables
    start = ThetaNode.from_coord(start, theta_resolution)
    goal = ThetaNode.from_coord(goal, theta_resolution)
    (xg, yg) = goal.continuous_state[:2]
    current = start
    open_vertices = set()
    closed_vertices = {current}
    came_from = dict()
    came_from_paths = list()
    f_scores = dict()
    g_scores = dict([(start, heuristic(start, goal))])
    start.g_score = heuristic(start, goal)

    x_size = _map.x_size
    y_size = _map.y_size

    # Generate solution

    turn_radius = plane.turn_radius
    if not d:
        d = turn_radius
    if d < np.sqrt(2):
        d = np.sqrt(2)
    if d > 2 * turn_radius:
        d = 2 * turn_radius

    i = 0
    while current is not None:
        # Compute neighbours
        neighbours, paths, radii = get_hybrid_neighbours(current.continuous_state, _map, plane, search_resolution, d)
        for neighbour in neighbours:
            print('exploring neighbour:', neighbour)
            neighbour_node = ThetaNode.from_coord(neighbour, theta_resolution)
            neighbour_node.radius = radii[neighbour]
            # neighbour_node = transform_to_discrete_theta_node(neighbour, theta_resolution)
            print('neighbour xyt:', neighbour_node.xyt)
            if neighbour_node in closed_vertices:  # Node has already been explored exhaustively
                print('neighbour is closed')
                continue
            # h_score = heuristics.move_cost(paths[neighbour], _map.obstacles, plane.width)
            (h_score, neighbour_node.momentum) = heuristics.momentum_move_cost(paths[neighbour], radii[neighbour],
                                                                               _map.obstacles, plane.width,
                                                                               current.momentum, current.radius)
            g_score = g_scores[current] + h_score
            f_score = g_score + heuristic(neighbour_node, goal)
            # replace_checker = True
            # if neighbour_node not in open_vertices:  # Add to open vertices list after it's explored
            #     print('added to open vertices')
            #     open_vertices.add(neighbour_node)
            #     replace_checker = False  # Delete this neighbour_node's explored neighbours for continuity
            # elif g_score >= g_scores[neighbour_node]:  # Skip if g score is worse than the existing one
            #     continue
            #
            # # Update the neighbour's f score and where it comes from
            # if replace_checker is True:
            #     print('replacing...')
            #     clear_neighbours(neighbour_node, open_vertices, came_from)
            #     open_vertices.add(neighbour_node)
            if neighbour_node in open_vertices:
                if f_score < f_scores[neighbour_node]:
                    print('replacing...')
                    clear_neighbours(neighbour_node, open_vertices, came_from, came_from_paths)
                    open_vertices.add(neighbour_node)
                else:
                    print('g score is worse, skip')
                    continue
            else:
                open_vertices.add(neighbour_node)

            # f_score = g_score + heuristic(neighbour_node, goal)
            g_scores[neighbour_node] = g_score
            f_scores[neighbour_node] = f_score
            came_from[neighbour_node] = (current, paths[neighbour])
            came_from_paths.append(paths[neighbour])

            neighbour_node.g_score = g_score
            neighbour_node.f_score = f_score
            neighbour_node.came_from = current
            neighbour_node.path = paths[neighbour]

        # Find the next current node and check if goal is within one search distance
        if len(open_vertices) > 0:
            print('length of open vertices:', len(open_vertices))
            current = None
            current_end = None
            current_f = np.inf
            current_end_f = np.inf
            for node in open_vertices:
                print('xyt:', node.xyt)
                print('continuous:', node.continuous_state)
                (xn, yn) = node.continuous_state[:2]
                if np.sqrt(pow((xn - xg), 2) + pow((yn - yg), 2)) <= d / 2:  # goal within one search distance
                    if node.f_score < current_end_f:
                        current_end = node
                        current_end_f = node.f_score
                elif node.f_score < current_f:
                    current = node
                    current_f = node.f_score
            print('current.xyt:', current.xyt)
            print('current.continuous:', current.continuous_state)
            print('current theta range:', current.theta_range)
            print('-' * 20)
        else:
            raise RuntimeError('Hybrid A* failed to find a solution')

        # Check if goal is reached
        if current_end is not None:
            # path = [current_end.continuous_state[:2]]
            open_vertices.remove(current_end)
            closed_vertices.add(current_end)
            # visualize(_map, came_from_paths, start, goal, open_vertices, closed_vertices)
            path = arc_connect(current_end, goal, min_radius=plane.turn_radius)
            if heuristics.check_collision(path, _map.obstacles, plane.width) is False:
                # TODO: this step returns False if the arc_connect function returns None as a Path, which means,
                #   if the current end point is within the proximity of goal, and arc_connect cannot connect these two
                #   points, AND if there's an obstacle between the two points, the system will falsely consider that the
                #   goal has been reached and proceed to generate solution. This needs to be fixed.
                # if the final section of the path doesn't collide with the obstacles
                while path is None:
                    current_end = came_from[current_end][0]
                    path = arc_connect(current_end, goal, min_radius=plane.turn_radius)
                path.reverse()
                while current_end in came_from:
                    path_section = came_from[current_end][-1][:-1]
                    path_section.reverse()
                    path += path_section
                    print(path)
                    current_end = came_from[current_end][0]
                path.reverse()
                return path

        open_vertices.remove(current)
        closed_vertices.add(current)

        i += 1
        # if i > 500:
        #     visualize(_map, came_from_paths, start, goal, open_vertices, closed_vertices)


def spline_search(_map: Map, plane: Plane, start: Tuple[float, float, float], goal: Tuple[float, float, float],
                  heuristic=heuristics.euclidean, search_resolution: int = 6, d: float = None):
    """
    A slightly different search method than the hybrid search, it only returns the coordinates of each waypoint instead
    of the entire line (no extremely small interval between waypoints on curves)

    Params
    ------
    """
    theta_resolution = 36

    # Initialize variables
    start = ThetaNode.from_coord(start, theta_resolution)
    goal = ThetaNode.from_coord(goal, theta_resolution)
    (xg, yg) = goal.continuous_state[:2]
    current = start
    open_vertices = set()
    closed_vertices = {current}
    came_from = dict()
    came_from_paths = list()
    f_scores = dict()
    g_scores = dict([(start, heuristic(start, goal))])
    start.g_score = heuristic(start, goal)

    x_size = _map.x_size
    y_size = _map.y_size

    # Generate solution

    turn_radius = plane.turn_radius
    if not d:
        d = turn_radius
    if d < np.sqrt(2):
        d = np.sqrt(2)
    if d > 2 * turn_radius:
        d = 2 * turn_radius

    i = 0
    while current is not None:
        # Compute neighbours
        neighbours, paths, radii = get_hybrid_neighbours(current.continuous_state, _map, plane, search_resolution, d)
        for neighbour in neighbours:
            print('exploring neighbour:', neighbour)
            neighbour_node = ThetaNode.from_coord(neighbour, theta_resolution)
            neighbour_node.radius = radii[neighbour]
            # neighbour_node = transform_to_discrete_theta_node(neighbour, theta_resolution)
            print('neighbour xyt:', neighbour_node.xyt)
            if neighbour_node in closed_vertices:  # Node has already been explored exhaustively
                print('neighbour is closed')
                continue
            # h_score = heuristics.move_cost(paths[neighbour], _map.obstacles, plane.width)
            (h_score, neighbour_node.momentum) = heuristics.momentum_move_cost(paths[neighbour], radii[neighbour],
                                                                               _map.obstacles, plane.width,
                                                                               current.momentum, current.radius)
            g_score = g_scores[current] + h_score
            f_score = g_score + heuristic(neighbour_node, goal)
            # replace_checker = True
            # if neighbour_node not in open_vertices:  # Add to open vertices list after it's explored
            #     print('added to open vertices')
            #     open_vertices.add(neighbour_node)
            #     replace_checker = False  # Delete this neighbour_node's explored neighbours for continuity
            # elif g_score >= g_scores[neighbour_node]:  # Skip if g score is worse than the existing one
            #     continue
            #
            # # Update the neighbour's f score and where it comes from
            # if replace_checker is True:
            #     print('replacing...')
            #     clear_neighbours(neighbour_node, open_vertices, came_from)
            #     open_vertices.add(neighbour_node)
            if neighbour_node in open_vertices:
                if f_score < f_scores[neighbour_node]:
                    print('replacing...')
                    clear_neighbours(neighbour_node, open_vertices, came_from, came_from_paths)
                    open_vertices.add(neighbour_node)
                else:
                    print('g score is worse, skip')
                    continue
            else:
                open_vertices.add(neighbour_node)

            # f_score = g_score + heuristic(neighbour_node, goal)
            g_scores[neighbour_node] = g_score
            f_scores[neighbour_node] = f_score
            came_from[neighbour_node] = (current, paths[neighbour])
            came_from_paths.append(paths[neighbour])

            neighbour_node.g_score = g_score
            neighbour_node.f_score = f_score
            neighbour_node.came_from = current
            neighbour_node.path = paths[neighbour]

        # Find the next current node and check if goal is within one search distance
        if len(open_vertices) > 0:
            print('length of open vertices:', len(open_vertices))
            current = None
            current_end = None
            current_f = np.inf
            current_end_f = np.inf
            for node in open_vertices:
                print('xyt:', node.xyt)
                print('continuous:', node.continuous_state)
                (xn, yn) = node.continuous_state[:2]
                if np.sqrt(pow((xn - xg), 2) + pow((yn - yg), 2)) <= d / 2:  # goal within one search distance
                    if node.f_score < current_end_f:
                        current_end = node
                        current_end_f = node.f_score
                elif node.f_score < current_f:
                    current = node
                    current_f = node.f_score
            print('current.xyt:', current.xyt)
            print('current.continuous:', current.continuous_state)
            print('current theta range:', current.theta_range)
            print('-' * 20)
        else:
            raise RuntimeError('Hybrid A* failed to find a solution')

        # Check if goal is reached
        if current_end is not None:
            # path = [current_end.continuous_state[:2]]
            open_vertices.remove(current_end)
            closed_vertices.add(current_end)
            # visualize(_map, came_from_paths, start, goal, open_vertices, closed_vertices)
            path = arc_connect(current_end, goal, min_radius=plane.turn_radius)
            if heuristics.check_collision(path, _map.obstacles, plane.width) is False:
                # if the final section of the path doesn't collide with the obstacles
                # while path is None:
                #     current_end = came_from[current_end][0]
                #     path = arc_connect(current_end, goal, min_radius=plane.turn_radius)
                # path.reverse()
                # while current_end in came_from:
                #     path_section = came_from[current_end][-1][:-1]
                #     path_section.reverse()
                #     path += path_section
                #     print(path)
                #     current_end = came_from[current_end][0]
                path = [goal.continuous_state[:2]]
                while current_end in came_from:
                    path.append(current_end.continuous_state[:2])
                    current_end = came_from[current_end][0]
                path.append(start.continuous_state[:2])
                path.reverse()
                return path

        open_vertices.remove(current)
        closed_vertices.add(current)

        i += 1
        # if i > 500:
        #     visualize(_map, came_from_paths, start, goal, open_vertices, closed_vertices)


def clear_neighbours(node: ThetaNode, open_vertices, came_from, came_from_paths):
    """Clear all neighbours and sub-neighbours of a node"""
    for open_node in open_vertices:
        if came_from[open_node][0] == node:
            clear_neighbours(open_node, open_vertices, came_from, came_from_paths)
    open_vertices.remove(node)
    came_from_paths.remove(came_from[node][1])
    del (came_from[node])


def arc_connect(start: ThetaNode, goal: Node, resolution: float = np.pi / 180, min_radius: Optional[float] = None):
    """
    TODO: Add a check for obstacles, unnecessary at this moment, the check is now done outside this function during the
        search process
    Move from one node to another through an arc given their coordinate and the first node's heading
    """
    if hasattr(start, 'continuous_state') is False:
        raise RuntimeError("Starting node doesn't have a continuous coordinate")
    elif hasattr(goal, 'continuous_state') is False:
        raise RuntimeError("Ending node doesn't have a continuous coordinate")
    else:
        x1, y1, theta1 = start.continuous_state
        x2, y2 = goal.continuous_state[:2]

        v12 = (x2 - x1, y2 - y1)
        v1 = (np.cos(theta1), np.sin(theta1))
        theta1 = np.angle([complex(*v1)])
        theta12 = np.angle([complex(*v12)])

        d_theta = np.abs(theta1 - theta12)
        if d_theta >= np.pi / 2:
            return None

        if theta1 != theta12:
            turning_direction = -1 if np.cross(np.array(v1), np.array(v12)) < 0 else 1

            epsilon = np.pi / 2 - d_theta
            epsilon = np.abs(epsilon) % (2 * np.pi) * np.abs(epsilon) / epsilon

            angle = np.pi - 2 * epsilon
            radius = np.linalg.norm(v12) / (2 * np.cos(epsilon))
            if min_radius is not None and radius < min_radius:
                return None

            theta01 = theta1 - turning_direction * np.pi / 2
            theta02 = theta01 + turning_direction * angle
            x0 = x1 - radius * np.cos(theta01)
            y0 = y1 - radius * np.sin(theta01)

            path = list()
            for theta in np.arange(min(theta01, theta02), max(theta01, theta02), resolution):
                path.append((x0 + radius * np.cos(theta), y0 + radius * np.sin(theta)))

            if turning_direction > 0:
                path.append((x2, y2))
            else:
                path.append((x1, y1))
                path.reverse()
        else:
            path = [(x1, y1), (x2, y2)]

        return path


# def search(_map: Map, plane: Plane, start: Tuple[float, float, float], goal: Tuple[float, float, float],
#            heuristic=heuristics.euclidean):
#     """
#     Former developing search function for path solution using hybrid A* algorithm, replaced by hybrid_search, depleted
#     """
#     raise Warning('This function is depleted')
#     theta_resolution = 10
#
#     # Initialize variables
#     start = transform_to_discrete_theta_node(start, theta_resolution)
#     goal = transform_to_discrete_theta_node(goal, theta_resolution)
#     current = start
#     open_vertices = set()
#     closed_vertices = {current}
#     came_from = dict()
#     f_scores = dict()
#     g_scores = dict([(start, heuristic(start, goal))])
#
#     x_size = _map.x_size
#     y_size = _map.y_size
#
#     # Generate solution
#     while current is not None:
#         # Compute neighbours
#         neighbours, paths, radii = get_hybrid_neighbours(current.continuous_state, x_size, y_size, plane.turn_radius,
#                                                          3)
#         for neighbour in neighbours:
#             print('exploring neighbour:', neighbour)
#             neighbour_node = transform_to_discrete_theta_node(neighbour, theta_resolution)
#             if neighbour_node in closed_vertices:  # Node has already been explored exhaustively
#                 continue
#             g_score = g_scores[current] + heuristics.move_cost(paths[neighbour], _map.obstacles, plane.width)
#             if neighbour_node not in open_vertices:  # Add to open vertices list after it's explored
#                 open_vertices.add(neighbour_node)
#             elif g_score >= g_scores[neighbour_node]:  # Skip if g score is worse than the existing one
#                 continue
#
#             # Update the neighbour's f score and where it comes from
#             g_scores[neighbour_node] = g_score
#             f_scores[neighbour_node] = g_score + heuristic(neighbour_node, goal)
#             came_from[neighbour_node] = (current, paths[neighbour][:-1])
#
#         # Find the next current node
#         if len(open_vertices) > 0:
#             print('length of open vertices:', len(open_vertices))
#             current = None
#             current_f = np.inf
#             for node in open_vertices:
#                 if f_scores[node] < current_f:
#                     current = node
#                     current_f = f_scores[node]
#             print('current:', current.continuous_state)
#         else:
#             raise RuntimeError('Hybrid A* failed to find a solution')
#
#         # Check if goal is reached
#         if current.xy == goal.xy:
#             path = list()
#             while current in came_from:
#                 came_from[current][-1].reverse()
#                 path += came_from[current][-1]
#                 print(path)
#                 current = came_from[current][0]
#             path.reverse()
#             return path
#
#         open_vertices.remove(current)
#         closed_vertices.add(current)


def multiple_hybrid_search(_map: Map, plane: Plane, waypoints: List[Tuple[float, float, float]],
                           heuristic=heuristics.euclidean, search_resolution: int = 6, d: float = None):
    """
    Conduct a hybrid search for a list of waypoints

    Params
    ------

    _map : Map
        the map that the search is conducted in

    plane : Plane
        the plane used for the search

    waypoints : List[Tuple[float, float, float]]
        a list of waypoints that needs to be hit during the search, only the first waypoint's heading is relevant

    heuristic : function
        a heuristic function used to estimate the cost from the current node to the goal

    search_resolution : int
        defines the search density, passed to the turning_resolution argument in get_hybrid_neighbours() function

    d : float
        defines the search displacement in each step, passed to the d argument in get_hybrid_neighbours() function
    """
    start = waypoints[0]
    goals = waypoints[1:]
    path = list()

    for goal in goals:
        path_section = hybrid_search(_map, plane, start, goal, heuristic, search_resolution, d)
        (x1, y1) = path_section[-2]
        (x2, y2) = path_section[-1]
        if x2 - x1 != 0:
            n = (y2 - y1) / (x2 - x1)
            theta = np.arctan(n)
            if x2 < x1:
                theta += np.pi
        else:
            theta = np.pi / 2 if y2 > y1 else -np.pi / 2
        start = [float(x2), float(y2), float(theta)]
        path += path_section[:-1]
    return path


def multiple_spline_search(_map: Map, plane: Plane, waypoints: List[Tuple[float, float, float]],
                           heuristic=heuristics.euclidean, search_resolution: int = 6, d: float = None):
    """
    Conduct a spline search for a list of waypoints

    Params
    ------

    _map : Map
        the map that the search is conducted in

    plane : Plane
        the plane used for the search

    waypoints : List[Tuple[float, float, float]]
        a list of waypoints that needs to be hit during the search, only the first waypoint's heading is relevant

    heuristic : function
        a heuristic function used to estimate the cost from the current node to the goal

    search_resolution : int
        defines the search density, passed to the turning_resolution argument in get_hybrid_neighbours() function

    d : float
        defines the search displacement in each step, passed to the d argument in get_hybrid_neighbours() function
    """
    start = waypoints[0]
    goals = waypoints[1:]
    path = list()

    for goal in goals:
        path_section = spline_search(_map, plane, start, goal, heuristic, search_resolution, d)
        (x1, y1) = path_section[-2]
        (x2, y2) = path_section[-1]
        if x2 - x1 != 0:
            n = (y2 - y1) / (x2 - x1)
            theta = np.arctan(n)
            if x2 < x1:
                theta += np.pi
        else:
            theta = np.pi / 2 if y2 > y1 else -np.pi / 2
        start = [float(x2), float(y2), float(theta)]
        path += path_section[:-1]
    return path


if __name__ == '__main__':
    # Test start and goal
    test_start = (9, 1, math.pi * 3 / 4)
    test_goal = (8.5, 8.5, 0)

    # Test map information
    test_x_range = 10
    test_y_range = 10
    obstacle1 = [[7, 2], [6, 2], [5, 2], [4, 2], [3, 2], [2, 2], [2, 3], [2, 4], [3, 4], [4, 4], [4, 5],
                 [5, 5], [5, 6], [6, 6], [7, 6], [7, 5]]
    obstacle2 = [[10, 8], [9, 8], [8, 8], [7, 8], [6, 8], [5, 8], [4, 8], [4, 7], [3, 7], [3, 6]]
    obstacle3 = [(0, 0), (test_x_range, 0), (test_x_range, test_y_range), (0, test_y_range), (0, 0)]
    test_obstacles = [obstacle1, obstacle2, obstacle3]

    # Test plane parameter
    test_plane_width = 0.3
    test_turn_radius = 1.7

    # Test search parameter
    test_search_resolution = 5

    for test_start in [(5.5, 5.5, -np.pi / 2), (8.5, 0.5, np.pi / 4 * 3), (3.5, 3.5, 0)]:
        # Set up search
        test_plane = Plane(test_plane_width, test_turn_radius)
        test_map = Map(test_x_range, test_y_range, test_obstacles)
        resized_map, resized_plane, waypoints = resize(test_map, (1.3, 1.3), test_plane, [test_start[:2],
                                                                                          test_goal[:2]])
        test_start = waypoints[0]+[test_start[2]]
        test_goal = waypoints[1]+[test_goal[2]]
        test_path = hybrid_search(resized_map, resized_plane, test_start, test_goal,
                                  search_resolution=test_search_resolution)
        print(test_path)

        # Prepare plotting
        x_range = resized_map.x_size
        y_range = resized_map.y_size
        ax = plt.subplot()
        ax.title.set_text('turning radius=' + str(resized_plane.turn_radius))
        ax.set_xlim(-1, x_range + 1)
        ax.set_xticks(np.arange(0, x_range + 2, 1))
        ax.set_ylim(-1, y_range + 1)
        ax.set_yticks(np.arange(0, y_range + 2, 1))
        ax.grid('on')
        ax.set_aspect('equal', adjustable='box')

        # Plot the path
        ax.plot(*list(zip(*test_path)), 'b-')
        test_path = LineString(test_path).buffer(resized_plane.width)
        test_path = PolygonPatch(test_path, fc='tab:blue', ec='tab:blue', alpha=0.2, zorder=3)
        ax.add_patch(test_path)

        # Plot start and goal
        ax.scatter(*test_start[:-1], c='green')
        ax.scatter(*test_goal[:-1], c='red')

        # Plot obstacles
        for obstacle in resized_map.obstacles:
            ax.plot(*list(zip(*obstacle)), 'k-')
        plt.show()
