from hybrida.main import get_hybrid_neighbours
import matplotlib.pyplot as plt
import numpy as np
from hybrida.core import *
from hybrida.visualization import visualize

# Test map information
test_x_range = 10
test_y_range = 10
obstacle1 = [[8, 2], [7, 2], [6, 2], [5, 2], [4, 2], [3, 2], [2, 2], [2, 3], [2, 4], [3, 4], [4, 4], [4, 5],
             [5, 5], [5, 6], [6, 6], [7, 6], [7, 5]]
obstacle2 = [[10, 8], [9, 8], [8, 8], [7, 8], [6, 8], [5, 8], [4, 8], [4, 7], [3, 7], [3, 6]]
obstacle3 = [(0, 0), (test_x_range, 0), (test_x_range, test_y_range), (0, test_y_range), (0, 0)]
test_obstacles = [obstacle1, obstacle2, obstacle3]

# Test plane parameter
test_plane_width = 0.3
test_turn_radius = 1.5

# Test search parameter
current = (3.5, 2.5, 0)
turning_resolution = 4
d = 3

# Conduct test
test_plane = Plane(test_plane_width, test_turn_radius)
test_map = Map(test_x_range, test_y_range, test_obstacles)
neighbours, paths, radii = get_hybrid_neighbours(current, test_map, test_plane, turning_resolution, d)

# Visualize the result
visualize_paths = list()
for path in paths:
    visualize_paths.append(paths[path])

visualize(test_map, visualize_paths)

# ax = plt.subplot()
# ax.set_xlim(0, test_x_range)
# ax.set_xticks(np.arange(0, test_x_range + 1, 1))
# ax.set_ylim(0, test_y_range)
# ax.set_yticks(np.arange(0, test_y_range + 1, 1))
# ax.grid('on')
# ax.set_aspect('equal', adjustable='box')
#
# ax.scatter(*current[:2], c='green')
#
# i = 0
# for neighbour in neighbours:
#     path = paths[neighbour]
#     print(path)
#     ax.plot(*list(zip(*path)), 'b-')
#     ax.scatter(*neighbour[:2], c='red')
#     i += 1
# plt.show()
