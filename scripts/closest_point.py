import numpy as np


def closest_point(A, B, P):
    M = B - A
    to = np.dot(M, P - A) / np.dot(M, M)
    if to <= 0:
        closest_point = A
    elif 0 < to < 1:
        closest_point = A + to * M
    elif to >= 1:
        closest_point = B
    return closest_point