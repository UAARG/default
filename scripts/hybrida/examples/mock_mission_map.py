"""A map that contains several waypoints with obstacles in between"""
from hybrida.main import multiple_hybrid_search, multiple_spline_search
from hybrida.core import *
import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import LineString
from descartes import PolygonPatch


# map info
x_range = 20
y_range = 20

waypoint1 = (10, 7, 0)
waypoint2 = (18, 6, 0)
waypoint3 = (18, 3, 0)
waypoint4 = (11, 2, 0)
waypoint5 = (2, 3, 0)
waypoint6 = (2, 6, 0)
waypoint7 = (5, 8, 0)
waypoint8 = (10, 12, 0)
waypoint9 = (10, 18, 0)
waypoint10 = (14, 18, 0)
waypoint11 = (18, 14, 0)
waypoint12 = (10, 9, 0)
waypoint13 = (10, 7, 0)
waypoints = [waypoint1, waypoint2, waypoint3, waypoint4, waypoint5, waypoint6, waypoint7, waypoint8, waypoint9,
             waypoint10, waypoint11, waypoint12, waypoint13]

obstacle1 = [(9.5, 15.5), (9.5, 16.5), (10.5, 16.5), (10.5, 15.5), (9.5, 15.5)]
obstacle2 = [(16, 10), (16, 12), (14, 12), (14, 10), (16, 10)]
obstacle3 = [(13, 8), (19, 8), (19, 1), (9, 1), (1, 2), (1, 7), (4, 8), (8, 12), (8, 19), (14, 19), (19, 14), (12, 9),
             (13, 8)]
obstacles = [obstacle1, obstacle2, obstacle3]

# plane info
plane_width = 0.2
turn_radius = 0.5

# Conduct search
search_resolution = 4
plane = Plane(plane_width, turn_radius)
test_map = Map(x_range, y_range, obstacles)
path = multiple_hybrid_search(test_map, plane, waypoints, search_resolution=search_resolution)

# Prepare plotting
ax = plt.subplot()
ax.set_xlim(-1, x_range + 1)
ax.set_xticks(np.arange(0, x_range + 2, 1))
ax.set_ylim(-1, y_range + 1)
ax.set_yticks(np.arange(0, y_range + 2, 1))
ax.grid('on')
ax.set_aspect('equal', adjustable='box')

# Plot obstacles
for obstacle in obstacles:
    ax.plot(*list(zip(*obstacle)), 'k-')

# Plot the path
ax.plot(*list(zip(*path)), 'b-')
path = LineString(path).buffer(plane.width)
path = PolygonPatch(path, fc='tab:blue', ec='tab:blue', alpha=0.2, zorder=3)
ax.add_patch(path)

# Plot waypoints
for waypoint in waypoints:
    ax.scatter(*waypoint[:-1], c='green')

plt.show()
