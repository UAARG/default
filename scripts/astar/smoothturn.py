"""Contains various smooth turning algorithms, still under development, use smoothturn3 instead"""

import numpy as np
import math
import matplotlib.pyplot as plt


class SmoothTurnError(Exception):
    """
        Generic Python-exception-derived object raised by smoothturn functions.

        General purpose exception class, derived from Python's exception. Exception
        class, programmatically raised in smoothturn functions when a turning-related
        condition would prevent further correct execution of the function.

        Parameters
        ----------
        None

    """


def turn_on_radius(waypoints, turn_radius=0.0, wp_radius=0.0, n=100):
    """
    Calculate the path of the subject if it turn as the subject reaches certain distance to the targeted waypoint

    Parameters
    ----------
    waypoints : ndarray of floats
        Coordinates of three waypoints
    turn_radius : float
        Turn radius of the subject
    wp_radius : float
        Minimal distance to the middle waypoints that is used to determine whether the subject should start turning
    n : int
        Iteration cycles to adjust path accuracy

    Notes
    -----
    Returns a list of coordinates of the computed path

    """
    # Turn when plane reached a distance to the waypoints
    v2 = waypoints[2] - waypoints[1]
    v1 = waypoints[1] - waypoints[0]
    v = np.stack([v1, v2])

    # v1_a = math.sqrt(np.sum(v1 ** 2))
    # v2_a = math.sqrt(np.sum(v2 ** 2))
    #
    # i = v1 / v1_a
    # j = [-i[1], i[0]]
    # M = np.stack([i, j])
    #
    # v = np.matmul(M, v.T).T
    v = _transform(v, v1)

    angle = (np.pi - abs(_return_angle(v[1]))) / 2
    r_limit = np.tan(angle) * wp_radius
    if turn_radius < r_limit:
        path = turn_before_waypoint(waypoints, r_limit)
        return path

    else:
        if v[1, 1]:
            turn = 1 if v[1, 1] > 0 else -1
            vo1 = v[0] - [wp_radius, -turn * turn_radius]
            vstart = v[0] - [wp_radius, 0]

            # vo1 = np.matmul(np.linalg.inv(M), vo1.T).T
            vo1 = _inverse_transform(vo1, v1)
            o1 = waypoints[0] + vo1
            # vstart = np.matmul(np.linalg.inv(M), vstart.T).T
            vstart = _inverse_transform(vstart, v1)
            start = waypoints[0] + vstart

            step = 2 * np.pi / n
            theta = np.arange(0, 2 * np.pi, step)
            p = o1 + np.array([2 * turn_radius * np.cos(theta), 2 * turn_radius * np.sin(theta)]).T

            vp = p - waypoints[1]
            d = np.abs(np.cross(v2, vp)) / np.linalg.norm(v2)
            p = p[np.abs(d - turn_radius) <= 2 * turn_radius * np.sin(step)]

            v1 = waypoints[0] - waypoints[1]
            vp = p - waypoints[1]
            vo1 = o1 - waypoints[1]
            # i = v2 / v2_a
            # j = [-i[1], i[0]]
            # M = np.stack([i, j])
            # v1 = np.matmul(M, v1.T).T
            # vo1 = np.matmul(M, vo1.T).T
            # vp = np.matmul(M, vp.T).T
            v1 = _transform(v1, v2)
            vo1 = _transform(vo1, v2)
            vp = _transform(vp, v2)
            vp = vp[vp[:, 0] > vo1[0]]

            vp = vp[vp[:, 0] < np.linalg.norm(v2)]
            if vp.size == 0:
                raise SmoothTurnError("turn_on_radius overshoots")

            vp = vp[vp[:, 1] / v1[1] < 0]
            norm = np.linalg.norm(vp, axis=1)
            e = norm - turn_radius
            vp = vp[e == np.min(np.abs(e))][0]

            # vp = np.matmul(np.linalg.inv(M), vp.T).T
            vp = _inverse_transform(vp, v2)
            o2 = waypoints[1] + vp

            vstart1 = start - o1
            vend1 = o2 - o1
            astart1 = _return_angle(vstart1)
            aend1 = _return_angle(vend1)
            angle1 = _return_true_angle(astart1, aend1, turn)
            aend1 = astart1 + angle1
            step1 = abs(angle1) / n
            theta1 = np.arange(np.min([astart1, aend1]), np.max([astart1, aend1]), step1)
            p1 = o1 + np.array([turn_radius * np.cos(theta1), turn_radius * np.sin(theta1)]).T
            p1 = np.flipud(p1) if astart1 > aend1 else p1

            vstart2 = o1 - o2
            v3 = o2 - waypoints[1]
            vend2 = np.dot(v3, v2) / np.dot(v2, v2) * v2 - v3
            astart2 = _return_angle(vstart2)
            aend2 = _return_angle(vend2)
            angle2 = _return_true_angle(astart2, aend2, -turn)
            aend2 = astart2 + angle2
            step2 = abs(angle2) / n
            theta2 = np.arange(np.min([astart2, aend2]), np.max([astart2, aend2]), step2)
            p2 = o2 + np.array([turn_radius * np.cos(theta2), turn_radius * np.sin(theta2)]).T
            p2 = np.flipud(p2) if astart2 > aend2 else p2

            path = np.concatenate((np.expand_dims(waypoints[0], axis=0), p1, p2, np.expand_dims(waypoints[2], axis=0)))
            return path

        else:
            return waypoints


def turn_before_waypoint(waypoints, turn_radius=0.0, n=30):
    """
    Calculate the path of the subject if it turn before it reaches the targeted waypoints.
    This method will calculate where the plane needs to turn so that it can finish turning in one turn.

    Parameters
    ----------
    waypoints : ndarray of floats
        Coordinates of three waypoints
    turn_radius : float
        Turn radius of the subject
    n : int
        Iteration cycles to adjust path accuracy

    Notes
    -----
    Returns a list of coordinates of the computed path

    """
    v1 = waypoints[1] - waypoints[0]
    v2 = waypoints[2] - waypoints[1]
    v = np.stack([v1, v2])

    # v1_a = math.sqrt(np.sum(v1 ** 2))
    # v2_a = math.sqrt(np.sum(v2 ** 2))
    #
    # i = v1 / v1_a
    # j = [-i[1], i[0]]
    # M = np.stack([i, j])
    # v = np.matmul(M, v.T).T
    v = _transform(v, v1)

    if v[1, 1]:
        turn = 1 if v[1, 1] > 0 else -1
        theta = 1 / 2 * (math.pi - math.acos(v[1, 0] / math.sqrt(np.sum(v[1] ** 2))))
        d = turn_radius / math.tan(theta)
        vp1 = v[0] - [d, 0]
        v2_a = np.linalg.norm(v2)
        vp2 = v[0] + v[1] * d / v2_a
        o = v[0] - [d, -turn * turn_radius]
        vp = np.stack((vp1, vp2, o))
        # vp = np.matmul(np.linalg.inv(M), vp.T).T
        vp = _inverse_transform(vp, v1)
        p = vp + waypoints[0]

        a = vp[:-1] - vp[-1]
        i = a[0] / turn_radius
        j = [-i[1], i[0]]
        M = np.stack([i, j])
        beta_max = (math.pi - 2 * theta)
        beta = np.arange(0.0, beta_max + beta_max / n, beta_max / n) * turn
        varc = np.stack([np.cos(beta), np.sin(beta)]) * turn_radius
        varc = np.matmul(np.linalg.inv(M), varc).T
        arc = (varc + p[-1])
        path = np.concatenate((np.expand_dims(waypoints[0], axis=0), arc, np.expand_dims(waypoints[2], axis=0)))
        return path

    else:
        return waypoints


def _return_angle(_v):
    return np.angle(_v[0] + 1j * _v[1])


def _return_true_angle(astart, aend, turn):
    angle = aend - astart
    if angle < -np.pi:
        angle += 2 * np.pi if turn > 0 else 0
    elif angle > np.pi:
        angle -= 2 * np.pi if turn < 0 else 0
    elif 0 > angle > -np.pi:
        angle += 2 * np.pi if turn > 0 else 0
    elif 0 < angle < np.pi:
        angle -= 2 * np.pi if turn < 0 else 0
    elif abs(angle) == np.pi:
        angle = np.pi if turn > 0 else -np.pi
    return angle


def _compute_m(v):
    """Compute the transform matrix used for _transform and _inverse_transform"""
    i = v / np.linalg.norm(v)
    j = [-i[1], i[0]]
    return np.stack([i, j])


def _transform(vs, v=None, m=None):
    """Rotate vs so that v is a horizontal vector"""
    m = m if m else _compute_m(v)
    return np.matmul(m, vs.T).T


def _inverse_transform(vs, v=None, m=None):
    """Inverse the transform result of _transform"""
    m = m if m else _compute_m(v)
    return np.linalg.solve(m, vs.T).T


def plot(paths, waypoints):
    # TODO: show paths with different waypoints
    """
    An debugging tool that visualizes the paths

    Parameters
    ----------
    paths : ndarray of float
        A list of paths to be plotted
    waypoints : ndarray of int
        The waypoints that were used to generate the paths
    """
    fig_per_row = 3
    count = len(paths)
    row = count // fig_per_row + 1 if count % fig_per_row else count // fig_per_row

    width = 12 if row > 1 else count * 4
    fig_per_row = fig_per_row if row > 1 else count

    plt.figure(figsize=(width, 3 * row))
    for i, path in enumerate(paths):
        min_path = np.min(paths[path], axis=0)
        max_path = np.max(paths[path], axis=0)
        min_waypoints = np.min(waypoints, axis=0)
        max_waypoints = np.max(waypoints, axis=0)
        _min = np.min(np.vstack([min_path, min_waypoints]), axis=0)
        _max = np.max(np.vstack([max_path, max_waypoints]), axis=0)
        _range = _max - _min
        if _range[0] > _range[1]:
            x_range = np.array([_min[0] - 1, _max[0] + 1], dtype=int)
            y_range = np.array([_min[1] - 1, _min[1] + 1 + _range[0]], dtype=int)
        else:
            x_range = np.array([_min[0] - 1, _min[0] + 1 + _range[1]], dtype=int)
            y_range = np.array([_min[1] - 1, _max[1] + 1], dtype=int)
        ax = plt.subplot(row, fig_per_row, i + 1)
        ax.scatter(*waypoints.T)
        ax.plot(*paths[path].T)
        ax.set_xlim(*x_range)
        ax.set_xticks(np.arange(*x_range, 1))
        ax.set_ylim(*y_range)
        ax.set_yticks(np.arange(*y_range, 1))
        ax.grid('on')
        ax.set_aspect('equal', adjustable='box')
        ax.title.set_text(path)
        plt.subplots_adjust(hspace=0.4, wspace=0.4)
    plt.show()


if __name__ == '__main__':
    test_points = np.array([(0, 5), (0, 0), (6, 3)])
    test_turn_radius = 1.2
    test_paths = {
        'turn_on_radius(0.8)': turn_on_radius(test_points, test_turn_radius, 0.8),
        'turn_on_radius(2)': turn_on_radius(test_points, test_turn_radius, 2),
        'turn_on_radius(0)': turn_on_radius(test_points, test_turn_radius, 0),
        'turn_before_waypoint': turn_before_waypoint(test_points, test_turn_radius)
    }
    plot(test_paths, test_points)



