class Spiral:
    
    def __init__(self, xOrigin,yOrigin, radius, points):
        self.xOrigin = xOrigin
        self.yOrigin = yOrigin
        self.maxRadius = radius
        self.numPoints = points
        self.generateSpiral()
    
    def generateSpiral(self):
        from math import sin
        from math import cos
        
        currentRadius = 0
        theta = 0
        
        self.listt = []
        
        while currentRadius < self.maxRadius:
    
            if currentRadius < self.maxRadius * (0.2):
                currentRadius = currentRadius + self.maxRadius * (3.33/self.numPoints)
            elif currentRadius < self.maxRadius * (0.4):
                currentRadius = currentRadius + self.maxRadius * (1.33/self.numPoints)
            elif currentRadius < self.maxRadius * (0.6):
                currentRadius = currentRadius + self.maxRadius * (1 / self.numPoints)
            elif currentRadius < self.maxRadius * (0.8):
                currentRadius = currentRadius + self.maxRadius * (0.74 / self.numPoints)
            else:
                currentRadius = currentRadius + self.maxRadius * (0.63 / self.numPoints)
    
            theta = theta + 0.2
            
            x = self.xOrigin + (currentRadius * cos(theta))
            y = self.yOrigin + (currentRadius * sin(theta))
    
            point = (x,y)
            self.listt.append(point)
    
    
    def getSpiral(self):
        return(self.listt)
    
newSpiral = Spiral(0,0,50,50)

points = newSpiral.getSpiral()

f = open("points.txt",'w')

for point in points:
    f.write("%s\n" % str(point))
    
f.close()
