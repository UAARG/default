class Node:
    """Custom class without __hash__ method"""

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.xy = (self.x, self.y)


class AdvancedNode(Node):
    """Custom hashable class with defined magic method"""
    t = None

    def __hash__(self):
        return hash(self.xy)

    def __eq__(self, other):
        if hasattr(other, 'xy'):
            return self.xy == other.xy
        else:
            return False


# # Hashing test for custom classes without __hash__ magic method
# node_a = Node(1, 2)
# test_set = {node_a}
# node_b = Node(1, 2)
#
# print(node_a in test_set)
# print(node_b in test_set)
# print(node_a == node_b)

# # Hashing test for custom classes with __hash__ magic method
# advanced_node_a = AdvancedNode(1, 2)
# test_set = {advanced_node_a}
# advanced_node_b = AdvancedNode(1, 2)
# print(advanced_node_a.xy)
# print(advanced_node_b.xy)
#
# print(advanced_node_a in test_set)
# print(advanced_node_b in test_set)
# print(advanced_node_a == advanced_node_b)
# print(advanced_node_a)

# # set().add() test for custom classes with __hash__ magic method
# a = AdvancedNode(1, 2)
# a.t = 1
# b = AdvancedNode(1, 2)
# b.t = 2
#
# test_set = {b}
# test_set.add(a)
# c = test_set.pop()
# print(c.t)
