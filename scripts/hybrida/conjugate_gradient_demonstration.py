import numpy as np
import matplotlib.pyplot as plt


# define function
def f(x, y):
    return 1/3 * np.power(x, 2) + 1/3 * np.power(y, 2) + 1/5 * x * y


# plot function with contour map
x = np.linspace(-5, 5, 25)
y = np.linspace(-5, 5, 25)
X, Y = np.meshgrid(x, y)
Z = f(X, Y)

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.contour3D(X, Y, Z, 50, cmap='viridis')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')

# define hessain function
Q = np.array([[2/3, 1/5], [1/5, 2/3]])
b = np.array([[0], [0]])


# define function in vector form
def fv(X):
    return 1/2 * np.linalg.multi_dot((X.T, Q, X)) - np.dot(X.T, b)


# define function's gradient in vector form
def dfv(X):
    return np.dot(Q, X)


# implement conjugate gradient descent
######################################

# generate initial guess
X1 = np.array([[-4], [1]])

# gradient descent
D1 = -dfv(X1)



plt.show()
