from hybrida.core import *
from hybrida.main import hybrid_search
from shapely.geometry import LineString
from descartes import PolygonPatch
import matplotlib.pyplot as plt


# Test start and goal
test_start = (9, 1, math.pi * 3 / 4)
test_goal = (8.5, 8.5, 0)

# Test map information
test_x_range = 10
test_y_range = 10
obstacle1 = [[7, 2], [6, 2], [5, 2], [4, 2], [3, 2], [2, 2], [2, 3], [2, 4], [3, 4], [4, 4], [4, 5],
             [5, 5], [5, 6], [6, 6], [7, 6], [7, 5]]
obstacle2 = [[10, 8], [9, 8], [8, 8], [7, 8], [6, 8], [5, 8], [4, 8], [4, 7], [3, 7], [3, 6]]
obstacle3 = [(0, 0), (test_x_range, 0), (test_x_range, test_y_range), (0, test_y_range), (0, 0)]
test_obstacles = [obstacle1, obstacle2, obstacle3]

# Test plane parameter
test_plane_width = 0.3
test_turn_radius = 1.7

# Test search parameter
test_search_resolution = 4

for test_start in [(5.5, 5.5, -np.pi / 2), (8.5, 0.5, np.pi / 4 * 3), (3.5, 3.5, 0)]:
    # Set up search
    test_plane = Plane(test_plane_width, test_turn_radius)
    test_map = Map(test_x_range, test_y_range, test_obstacles)
    test_path = hybrid_search(test_map, test_plane, test_start, test_goal, search_resolution=test_search_resolution)
    print(test_path)

    # Prepare plotting
    ax = plt.subplot()
    ax.title.set_text('turning radius=' + str(test_turn_radius))
    ax.set_xlim(-1, test_x_range + 1)
    ax.set_xticks(np.arange(0, test_x_range + 2, 1))
    ax.set_ylim(-1, test_y_range + 1)
    ax.set_yticks(np.arange(0, test_y_range + 2, 1))
    ax.grid('on')
    ax.set_aspect('equal', adjustable='box')

    # Plot the path
    ax.plot(*list(zip(*test_path)), 'b-')
    test_path = LineString(test_path).buffer(test_plane.width)
    test_path = PolygonPatch(test_path, fc='tab:blue', ec='tab:blue', alpha=0.2, zorder=3)
    ax.add_patch(test_path)

    # Plot start and goal
    ax.scatter(*test_start[:-1], c='green')
    ax.scatter(*test_goal[:-1], c='red')

    # Plot obstacles
    for obstacle in test_obstacles:
        ax.plot(*list(zip(*obstacle)), 'k-')
    plt.show()