from hybrida.main import arc_connect
from hybrida.core import *
import matplotlib.pyplot as plt
import math


a = ThetaNode.from_coord((1.5, 1.5, math.pi), 10)
b = Node(2, 2)
b.continuous_state = (2.5, 2.5)

path = arc_connect(a, b)

ax = plt.subplot()
ax.grid('on')
ax.set_aspect('equal', adjustable='box')
ax.plot(*list(zip(*path)), 'b-')
ax.scatter(*a.continuous_state[:2], c='green')
ax.scatter(*b.continuous_state, c='red')
plt.show()
