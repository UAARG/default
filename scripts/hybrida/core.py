from typing import List, Tuple
import math
import numpy as np


class Map:
    """
    Represents the environment the search algorithm is used in.
    Contains information about the position of the obstacles, boundaries, and such.
    """

    def __init__(self, x_size: int, y_size: int, obstacles: List[List] = None):
        self.x_size = int(x_size)
        self.y_size = int(y_size)
        self.obstacles = obstacles


class Node:
    """
    TODO: some of the attributes created during __init__() is unnecessary, e.g., the momentum attribute, should probably
        be deleted
    Common nodes in simple A* algorithm with discretized coordinates

    Attributes
    ----------

    x : int
        discretized x coordinate of the node

    y : int
        discretized y coordinate of the node

    xy : Tuple[int ,int]
        discretized xy coordinate of the node

    g_score : float
        actual cost moving from the start to this node

    f_score : float
        estimated cost moving from the start through this node to the goal

    came_from : Node
        the node that this node comes from

    path : List[Tuple[float ,float]]
        the path that connects this node and the node it comes from

    continuous_state : Tuple[float ,float]
        the continuous coordinate of this node

    momentum : int
        used to calculate this node's "momentum" when this node's move cost using heuristics.momentum_move_cost()

    """

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.xy = (self.x, self.y)
        self.g_score = None
        self.f_score = None
        self.came_from = None
        self.path = None
        # self.neighbours = list()
        self.continuous_state = (None, None)
        self.momentum = 0

    # Make the class object hashable so that it can be added into a set
    def __hash__(self):
        return hash(self.xy)

    def __eq__(self, other):
        if hasattr(other, 'xy'):
            return self.xy == other.xy
        else:
            return False


class ThetaNode(Node):
    """
    Nodes with discretized coordinates used for hybrid A* algorithm that specify a range of heading in the node.
    Basically it divides a simple Node into several ThetaNode with the same discretized xy coordinates but different
    headings.

    Attributes
    ----------

    x : int
        discretized x coordinate of the node

    y : int
        discretized y coordinate of the node

    theta_order : int
        specify which interval of heading this node represent

    theta_interval : float
        specify the range of one interval, theta interval is pi/3 if the node is divided into 6 different headings,
        corresponding attribute in degree: theta_interval_d

    theta_min : float
        minimum heading angle in this node, corresponding attribute in degree: theta_min_d

    theta_max : float
        maximum heading angle in this node, corresponding attribute in degree: theta_max_d

    theta_range : Tuple[float, float]
        range of heading angle in this node, corresponding attribute in degree: theta_range_d

    xyt : Tuple[int, int, int]
        discretized xy coordinate and the heading (theta_order) of the node

    continuous_state : Tuple[float ,float, float]
        the continuous coordinate and heading of this node

    g_score : float
        actual cost moving from the start to this node

    f_score : float
        estimated cost moving from the start through this node to the goal

    came_from : Node
        the node that this node comes from

    path : List[Tuple[float ,float]]
        the path that connects this node and the node it comes from

    radius : float
        the turning radius of the path leading to this node, equals to math.inf if the path is straight

    momentum : int
        used to calculate this node's "momentum" when this node's move cost using heuristics.momentum_move_cost()

    """

    @classmethod
    def from_coord(cls, coord: Tuple[float, float, float], theta_resolution: int):
        """
        TODO: In cases where theta<0 is really close to 0, the operation: theta = theta + 2 * math.pi results in a theta
            larger than 2 * math.pi, needs to solve this, suspect: systematic loss of accuracy of float variable
            example: (6.319077862357725, 3.5260604299770058, -2.220446049250313e-16)
        Creates a ThetaNode object from a point given its coordinate and heading

        Params
        ------

        coord : Tuple[float, float, float]
            coordinate and heading of the point

        theta_resolution : int
            how many headings should one node be divided into

        Return
        ------
        theta_node : ThetaNode
            a ThetaNode object representing the point
        """
        x = int(coord[0])
        y = int(coord[1])
        theta = np.float((coord[2]))
        if theta == 0:
            theta_order = 0
        else:
            theta = abs(theta) % (2 * math.pi) * abs(theta) / theta
            theta = theta + 2 * math.pi if theta < 0 else theta
            theta_order = theta // (math.pi * 2 / theta_resolution)
        theta_node = ThetaNode(x, y, theta_order, theta_resolution)
        theta_node.continuous_state = coord
        return theta_node

    def __init__(self, x: int, y: int, theta_order: int, theta_resolution: int):
        super().__init__(x, y)

        self.theta_order = theta_order % theta_resolution

        self.theta_interval = math.pi * 2 / theta_resolution
        self.theta_min = self.theta_order * self.theta_interval
        self.theta_max = self.theta_min + self.theta_interval
        self.theta_range = (self.theta_min, self.theta_max)

        self.theta_interval_d = self.theta_interval * 180 / math.pi
        self.theta_min_d = self.theta_min * 180 / math.pi
        self.theta_max_d = self.theta_max * 180 / math.pi
        self.theta_range_d = (self.theta_min_d, self.theta_max_d)

        self.xyt = (self.x, self.y, self.theta_order)
        self.continuous_state = (None, None, None)

        self.radius = 0

    def __hash__(self):
        return hash(self.xyt)

    def __eq__(self, other):
        if hasattr(other, 'xyt'):
            return self.xyt == other.xyt
        else:
            return False


class Plane:
    """
    Plane class used in the search

    Attrs
    -----

    width : float
        total width of the plane

    turn_radius : float
        minimum turning radius of the plane
    """

    def __init__(self, width: float = 0.0, max_turn_radius: float = 0.0):
        self.width = width
        self.turn_radius = max_turn_radius
