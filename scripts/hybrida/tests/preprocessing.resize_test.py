from hybrida.core import *
from hybrida.preprocessing import resize
import matplotlib.pyplot as plt


obstacle1 = [[7, 2], [6, 2], [5, 2], [4, 2], [3, 2], [2, 2], [2, 3], [2, 4], [3, 4], [4, 4], [4, 5], [5, 5], [5, 6],
             [6, 6], [7, 6], [7, 5]]
obstacle2 = [[10, 8], [9, 8], [8, 8], [7, 8], [6, 8], [5, 8], [4, 8], [4, 7], [3, 7], [3, 6]]
obstacle3 = [(0, 0), (10, 0), (10, 10), (0, 10), (0, 0)]
test_obstacles = [obstacle1, obstacle2, obstacle3]

test_map = Map(10, 10, test_obstacles)
resized_map = resize(test_map, (1.6, 1.6))
x_range = resized_map.x_size
y_range = resized_map.y_size

ax = plt.subplot()
ax.set_xlim(-1, x_range + 1)
ax.set_xticks(np.arange(0, x_range + 2, 1))
ax.set_ylim(-1, y_range + 1)
ax.set_yticks(np.arange(0, y_range + 2, 1))
ax.grid('on')
ax.set_aspect('equal', adjustable='box')

for obstacle in resized_map.obstacles:
    ax.plot(*list(zip(*obstacle)), 'k-')

plt.show()