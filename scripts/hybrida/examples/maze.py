"""A map of a maze, with two different sizes"""
from hybrida.main import hybrid_search
from hybrida.core import *
import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import LineString
from descartes import PolygonPatch


# # Big maze information
# x_range = 100
# y_range = 50
#
# start = (5, 50, -np.pi / 2)
# goal = (95, 0, -np.pi / 2)
#
# obstacle1 = [(0, 50), (0, 0), (90, 0)]
# obstacle2 = [(10, 50), (100, 50), (100, 0)]
# obstacle3 = [(0, 20), (10, 20)]
# obstacle4 = [(40, 0), (40, 10)]
# obstacle5 = [(60, 0), (60, 10)]
# obstacle6 = [(100, 10), (80, 10)]
# obstacle7 = [(100, 30), (90, 30), (90, 20)]
# obstacle8 = [(30, 50), (30, 30)]
# obstacle9 = [(20, 40), (10, 40), (10, 30), (40, 30), (40, 20), (30, 20), (30, 10), (10, 10)]
# obstacle10 = [(20, 20), (20, 30)]
# obstacle11 = [(50, 20), (50, 10), (70, 10), (70, 20)]
# obstacle12 = [(70, 30), (70, 40), (90, 40)]
# obstacle13 = [(40, 40), (50, 40), (50, 30), (60, 30), (60, 20), (80, 20), (80, 40)]
# obstacles = [obstacle1, obstacle2, obstacle3, obstacle4, obstacle5, obstacle6, obstacle7, obstacle8, obstacle9,
#              obstacle10, obstacle11, obstacle12, obstacle13]

# Small maze information
x_range = 10
y_range = 5

start = (0.5, 5, -np.pi / 2)
goal = (9.5, 0, -np.pi / 2)

obstacle1 = [(0, 5), (0, 0), (9, 0)]
obstacle2 = [(1, 5), (10, 5), (10, 0)]
obstacle3 = [(0, 2), (1, 2)]
obstacle4 = [(4, 0), (4, 1)]
obstacle5 = [(6, 0), (6, 1)]
obstacle6 = [(10, 1), (8, 1)]
obstacle7 = [(10, 3), (9, 3), (9, 2)]
obstacle8 = [(3, 5), (3, 3)]
obstacle9 = [(2, 4), (1, 4), (1, 3), (4, 3), (4, 2), (3, 2), (3, 1), (1, 1)]
obstacle10 = [(2, 2), (2, 3)]
obstacle11 = [(5, 2), (5, 1), (7, 1), (7, 2)]
obstacle12 = [(7, 3), (7, 4), (9, 4)]
obstacle13 = [(4, 4), (5, 4), (5, 3), (6, 3), (6, 2), (8, 2), (8, 4)]
obstacles = [obstacle1, obstacle2, obstacle3, obstacle4, obstacle5, obstacle6, obstacle7, obstacle8, obstacle9,
             obstacle10, obstacle11, obstacle12, obstacle13]

# Plane information
plane_width = 0.2
turn_radius = 0.5

# Conduct search
search_resolution = 10
plane = Plane(plane_width, turn_radius)
test_map = Map(x_range, y_range, obstacles)
path = hybrid_search(test_map, plane, start, goal, search_resolution=search_resolution)

# Prepare plotting
ax = plt.subplot()
ax.set_xlim(-1, x_range + 1)
# ax.set_xticks(np.arange(0, x_range + 2, 1))
ax.set_ylim(-1, y_range + 1)
# ax.set_yticks(np.arange(0, y_range + 2, 1))
# ax.grid('on')
ax.set_aspect('equal', adjustable='box')

# Plot obstacles
for obstacle in obstacles:
    ax.plot(*list(zip(*obstacle)), 'k-')

# Plot the path
ax.plot(*list(zip(*path)), 'b-')
path = LineString(path).buffer(plane.width)
path = PolygonPatch(path, fc='tab:blue', ec='tab:blue', alpha=0.2, zorder=3)
ax.add_patch(path)

# Plot start and goal
ax.scatter(*start[:-1], c='green')
ax.scatter(*goal[:-1], c='red')

plt.show()
