import math


class Loiter:
    """
    Loiter(safety_distance, airspeed, alt, **kwargs) -> Loiter

    A class used to calculate loiter radius given safety distance, airspeed and altitude

    Methods
    ----------
    loiter(safety_distance=None, airspeed=None, alt=None, distance_safety_factor=None)
        calculate a flight path using given parameters
    """

    def __init__(self, safety_distance: int, airspeed: float, alt: int, radius_range=(50, 275), roll_limit=90,
                 distance_safety_factor=1.0, g=9.81, kill=False):
        """
        :param
        ----------
        All attributes are in SI units unless specified otherwise

        safety_distance : int
            the shortest distance allowed between the UAV and POI
        airspeed : float
            the airspeed of the UAV
        alt : int
            the altitude of the UAV
        radius_range : Tuple[int, int] = (50, 275)
            the range of allowable radius value
        roll_limit : int = 90
            the maximum value of allowable roll in degrees
        distance_safety_factor : float = 1.0
            safety factor for distance
        g : float = 9.81
            acceleration due to gravity
        kill : Boolean = False
            If set to True, will raise Exception when encounter unexpected errors
        """
        self.safety_distance = safety_distance
        self.airspeed = airspeed
        self.alt = alt
        self.radius_range = radius_range
        self.roll_limit = roll_limit
        self.safety_distance *= distance_safety_factor if distance_safety_factor else 1
        self.distance_safety_factor = distance_safety_factor if distance_safety_factor else 1.0
        self.g = g

        # This is the state of the solution or the state of radius and roll
        # self.OPTIMAL means the solution is the best solution, shortest distance to POI
        # A value smaller than self.OPTIMAL indicates that the solution is acceptable
        # A value larger than self.OPTIMAL indicates that such solution is unacceptable(maybe)
        self.OPTIMAL = 0
        self.SAFE = -1
        self.UNDER = 1
        self.OVER = 2
        self.DNE = 4

        # If loiter() return this value, then the state of solution is not updated
        self.NO_UPDATE_ERROR = 1000

        # determine whether to raise error when encounter unexpected error
        self.kill = kill

    def loiter(self, safety_distance: int = None, airspeed: float = None, alt: int = None,
               distance_safety_factor: float = None):
        """
        loiter(**kwargs) -> Tuple[float, float, int, str]

        Main computation function that returns loiter radius, distance to POI, solution_state and a message.

        :param
        ----------
        safety_distance : int
            the shortest distance allowed between the UAV and POI
        airspeed : float
            the airspeed of the UAV
        alt : int
            the altitude of the UAV
        distance_safety_factor : float = 1.0
            safety factor for distance

        :returns
        ----------
        radius : float
            loiter radius
        distance : float
            distance to POI
        solution_state : int
            solution_state=0 indicates optimal solution, solution_state<0 indicates acceptable solution,
            solution_state>0 indicates unacceptable solution, solution_state>100 indicates error
        message : str
            a short message describing the solution
        """

        self.airspeed = airspeed if airspeed else self.airspeed
        self.alt = alt if alt else self.alt
        self.safety_distance = safety_distance if safety_distance else self.safety_distance
        self.safety_distance *= distance_safety_factor if distance_safety_factor else self.distance_safety_factor

        # Initialize a error_state value in case an unexpected error happened and solution state is generated
        return_message = None, None, self.NO_UPDATE_ERROR

        # Compute radius and roll of the optimal solution from given parameter
        radius = math.pow(self.airspeed, 2) * self.alt / (self.safety_distance * self.g)
        roll = self.compute_roll(radius)

        # Check if radius and roll meet requirements
        radius_state = self.check_radius(radius)
        roll_state = self.check_roll(roll)

        # Based on the current value of radius and roll, generate different return values
        if radius_state == roll_state:
            # There's no lower limit for roll, so the only possible cases are:
            # radius_state == roll_state == self.SAFE
            # radius_state == roll_state == self.OVER
            if radius_state == self.SAFE:
                # Optimal solution
                distance = self.compute_distance(radius)
                return_message = radius, distance, self.OPTIMAL
            else:
                # Both radius and roll exceeds maximum value, need to change airspeed and altitude
                # TODO: Add implementation for different airspeed and altitude?
                return_message = None, None, self.OVER

        elif radius_state == self.OVER:
            # Radius exceeds maximum value but roll is acceptable
            # Solution: Change radius to maximum acceptable value then recheck new roll value
            return_message = self.set_radius_maximum()

        elif radius_state == self.UNDER:
            # Radius is lower than minimum value, roll state unknown
            # Solution: Change radius to minimum acceptable value then recheck new roll value
            return_message = self.set_radius_minimum(0)

        elif radius_state == self.SAFE:
            # Radius is acceptable but roll exceeds maximum value
            # Solution: Change roll to maximum acceptable value then recheck new radius value
            return_message = self.set_roll_maximum(0)

        # Change the solution state in to messages
        state = return_message[-1]
        if state == self.OPTIMAL:
            message = 'Optimal solution. Shortest distance to POI.'
        elif state == self.SAFE:
            message = 'Modified from optimal solution to suit radius and roll requirements.'
        elif state == self.OVER:
            message = '''Resulted radius and roll are higher than maximum values.
            You can solve this by lowering your airspeed and increasing your altitude.(maybe)'''
        elif state == self.DNE:
            message = 'No available path for current airspeed and altitude. Solution: not resolved yet.'
        else:  # Solution state is not updated or updated into an unexpected value
            if self.kill:
                raise Exception('An unexpected error has occurred! loiter solution state is not updated!')
            else:
                message = 'An unexpected error has occurred!'

        # Generate and return data
        return_message = return_message[:-1] + (state, message)
        return return_message

    def compute_distance(self, radius):
        """Compute the distance between the UAV and POI"""
        return radius + self.safety_distance

    def compute_roll(self, radius):
        """Compute the roll of the UAV"""
        return math.atan(math.pow(self.airspeed, 2) / (radius * self.g)) * (180 / math.pi)

    def check_radius(self, radius):
        """Check if radius exceeds its given range and return corresponding state signal"""
        if self.radius_range[1] is not None and radius > self.radius_range[1]:
            radius_state = self.OVER
        elif self.radius_range[0] is not None and radius < self.radius_range[0]:
            radius_state = self.UNDER
        else:
            radius_state = self.SAFE
        return radius_state

    def check_roll(self, roll):
        """Check if roll exceeds maximum and return corresponding state signal"""
        if self.roll_limit and roll > self.roll_limit:
            roll_state = self.OVER
        else:
            roll_state = self.SAFE
        return roll_state

    def set_radius_maximum(self):
        """Set radius value to maximum then recheck new roll value"""
        radius = self.radius_range[1]
        roll = self.compute_roll(radius)
        roll_state = self.check_roll(roll)
        if roll_state > self.OPTIMAL:
            # If roll is over maximum value since radius decreases
            return_message = None, None, self.DNE
        else:
            # Solution is acceptable, compute distance
            distance = self.compute_distance(radius)
            return_message = radius, distance, self.SAFE
        return return_message

    def set_radius_minimum(self, recursion_counter=None):
        """Set radius value to minimum then recheck new roll value"""
        self.check_infinite_recursion(recursion_counter)  # Used to check unexpected recursion
        radius = self.radius_range[0]
        roll = self.compute_roll(radius)
        roll_state = self.check_roll(roll)
        if roll_state > self.OPTIMAL:
            # Radius at minimum, roll exceeds maximum value
            # Solution: change roll to maximum acceptable value, then recheck new radius value
            return_message = self.set_roll_maximum(recursion_counter)
        else:
            # Solution is acceptable
            distance = self.compute_distance(radius)
            return_message = radius, distance, self.SAFE
        return return_message

    def set_roll_maximum(self, recursion_counter=None):
        """Set roll value to maximum then recheck new radius value"""
        self.check_infinite_recursion(recursion_counter)  # Used to check unexpected recursion
        roll = self.roll_limit
        radius = math.pow(self.airspeed, 2) / (self.g * math.tan(roll * (math.pi / 180)))
        radius_state = self.check_radius(radius)
        if radius_state > self.OPTIMAL:
            if radius_state == self.OVER:
                # If radius is over maximum value since roll decreases
                return_message = None, None, self.DNE
            else:
                # If radius is lower than minimum value after roll decreases
                # But this probably won't happen
                # But if it does, the program might run into a dead loop
                # The following function is in place to raise an error
                return_message = self.set_radius_minimum(recursion_counter + 1)
        else:
            # Solution is acceptable
            distance = self.compute_distance(radius)
            return_message = radius, distance, self.SAFE
        return return_message

    @staticmethod
    def check_infinite_recursion(recursion_counter):
        """Raise an error if the program runs into infinite recursion."""
        if recursion_counter >= 3:
            raise Exception("There is a potential risk of recursion! "
                            "set_roll_maximum() shouldn't have called set_radius_minimum()")


def main():
    safety_distance = 100
    airspeed = 24.0
    alt = 100
    roll_limit = 60
    radius_range = (50, 275)
    safety_factor = 1.05

    loiter = Loiter(safety_distance, airspeed, alt, radius_range, roll_limit, safety_factor)
    (radius, distance, state, message) = loiter.loiter()
    if radius and distance:
        print('radius = %dm, distance = %dm.' % (radius, distance))
        print(message)
    else:
        # No value for radius or distance is returned if there's no solution
        print(message)
        # IMPORTANT! Check state after Loiter.loiter()
        # If state>100, there's an unexpected error, error and exception handling is recommended


if __name__ == '__main__':
    main()
