import matplotlib.pyplot as plt
import numpy.linalg as linalg
import numpy as np
import string
from Loiter import Loiter

# Find the closest ordered pair on a geofence segment to the imaging location
def closest_point(A,B,P):
    M = B - A
    to = np.dot(M, P - A) / np.dot(M, M)
    if to <= 0:
        closest_point = A
    elif 0 < to < 1:
        closest_point = A + to * M
    elif to >= 1:
        closest_point = B
    return closest_point

# Calculate the distance between two ordered pairs
def distance(A, B):
    return linalg.norm(B - A)

# Waypoints and imaging location | Testing
waypoints = [(15,9), (8,10), (2,10), (4,5), (20,3),(19,8)]
imaging_location = (15,12)

# Find the closest ordered pair to the imaging location on each
# geofence segment and calculate the distance between them

waypoints.append(waypoints[0])
closest_points = []
x1_vectors = []
x1_distances = []
for i in range(1, len(waypoints)):
    A = np.array(waypoints[i -1])
    B = np.array(waypoints[i])
    P = np.array(imaging_location)
    C = closest_point(A,B,P)
    closest_points.append(C)
    x1_vectors.append(C - P)
    x1_distances.append(distance(P,C))

# Find the shortest distance and its vector to imaging point

#Sending to Liyu
safety_distance = min(x1_distances)
j = x1_distances.index(safety_distance)
x1_vector = x1_vectors[j]
closest_pair = closest_points[j]

airspeed = 24.0
alt = 100
radius_range = (50, 275)
calculations = Loiter(safety_distance, airspeed, alt, radius_range, kill=True)
radius, dist, state, message = calculations.loiter()

print(dist)
print(radius)

# UAV Baking location
UAV = imaging_location + dist*(x1_vector)/safety_distance
print("UAV Banking location = {}\n".format(UAV))


# Plot

# Reading waypoints
x, y = map(list, zip(*waypoints))

# Geofence plot
plt.subplot(1,2,1)
plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.plot(x,y,'k')
plt.grid()
################
plt.xlim([0,25]) 
plt.ylim([0,15])
################

# Imaging location | Geofence plot
plt.plot(imaging_location[0], imaging_location[1], 'bo')
plt.annotate("Imaging Location", (imaging_location[0], imaging_location[1]))

for k in range(0, len(closest_points)):
    plt.plot(closest_points[k][0], closest_points[k][1], "ro")
    plt.annotate("{}".format(x1_distances[k]), (closest_points[k][0], closest_points[k][1]))
    UAV_plot = imaging_location + dist*(x1_vectors[k])/x1_distances[k]
    plt.plot(UAV_plot[0], UAV_plot[1], "yo")
    x_lines = [UAV_plot[0], imaging_location[0]]
    y_lines = [UAV_plot[1], imaging_location[1]]
    plt.plot(x_lines, y_lines)

# Solution
plt.subplot(1,2,2)
plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.plot(x,y,'k')
plt.grid()
################
plt.xlim([0,25]) 
plt.ylim([0,15])
################

# Imaging location | Closest points plot
plt.plot(imaging_location[0], imaging_location[1], 'bo')
plt.annotate("Imaging Location", (imaging_location[0], imaging_location[1]))

# Label each ordered pair
for index in range(len(waypoints) - 1):
    plt.annotate(string.ascii_uppercase[index], (x[index],y[index]))

plt.annotate("  UAV Banking location", (UAV[0], UAV[1]))
plt.plot(UAV[0], UAV[1], "yo")
plt.plot(closest_pair[0], closest_pair[1], "ro")
plt.annotate("      {}".format(safety_distance), (closest_pair[0], closest_pair[1]))

# Plotting lines between closest point and imaging location
x_lines = [UAV[0], imaging_location[0]]
y_lines = [UAV[1], imaging_location[1]]
plt.plot(x_lines, y_lines)


plt.show()