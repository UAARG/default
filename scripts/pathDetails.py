# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 22:22:45 2020

@author: virkS
"""

import math

"""
Takes tuples as point inputs and generates the distances between each succesive 
pair of points as well as the angles between each points. Angles are measured 
as "how far counter-clockwise do I need to turn to point towards the next point".

Assumed plane starts by pointing in the +y direction.
"""
class pathDetails:
    
    def __init__(self,points):
        
        self.xPoints = [point[0] for point in points]
        self.yPoints = [point[1] for point in points]
        self.generateDetails()
    
    def generateDetails(self):
        
        theta = 0
        direction = 90
        
        self.distances = []
        self.angles = []
        
        for n in range(1,len(self.xPoints)):
            deltaY = self.yPoints[n] - self.yPoints[n-1]
            deltaX = self.xPoints[n] - self.xPoints[n-1]
            
            d = (deltaX**2 + deltaY**2)**0.5
            self.distances.append(d)
            
            theta = self.findTheta(deltaY,deltaX,d)
            
            self.angles.append(theta - direction)
            
            direction = theta
            
    def findTheta(self,y,x,h):
        
        sin = y / h
        
        cos = x / h
        
        if sin == 0:
            if cos == 1:
                theta = 0
            else:
                theta = 180
        elif cos == 0:
            if sin == 1:
                theta = 90
            else:
                theta = 270
                
        else:
            theta = math.degrees(math.atan2(y,x))
        
        return(theta)
        
    
    def getDistances(self):
        return(self.distances)
    
    
    def getAngles(self):
        return(self.angles)
        

path = pathDetails([(0,0),(1,1),(-3,4),(2,-3)])

a = path.getAngles()  

d = path.getDistances()

print(a)

print(d)          
            
            